---
layout: markdown_page
title: "Contributing to Documentation"
description: "This section pertains to documentation changes that are independent of other code/feature changes."
canonical_path: "/community/contribute/documentation/"
---

## Documentation

If you're updating documentation as part of developing code, go to the [Contributing to Development page](/community/contribute/development/index.html).

- View the [documentation issues curated specifically for new contributors](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=good%20for%20new%20contributors&label_name%5B%5D=Accepting%20merge%20requests&label_name%5B%5D=documentation).

  - Pick an issue you'd like to work on. In the issue, check out the following:
    - If another contributor has already been assigned to the issue, pick a different one to work on.
    - If no contributor has been assigned to the issue, leave a comment that you want to pick it up, and mention `@gl-docsteam` for it to be assigned to you. Here's a message you can copy and paste in a comment:

      ```plaintext
      Hi @gl-docsteam, I'd like to work on this issue. Please assign it to me. Thanks!
      ``` 

- If you don't find an issue you'd like to work on, you can still open merge requests.

  - Try installing and running the [Vale linting tool](https://docs.gitlab.com/ee/development/documentation/testing.html#vale)
  and fix issues it finds.
  - Select Edit at the bottom of any page on docs.gitlab.com.
  - Or just look through pages [in the `/doc` directory](https://gitlab.com/gitlab-org/gitlab/-/tree/master/doc)
  until you find one you'd like to improve.

If you've never contributed to the GitLab documentation before, [view the instructions](https://docs.gitlab.com/ee/development/documentation/workflow.html).
